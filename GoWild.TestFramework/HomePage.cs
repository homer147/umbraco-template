﻿using OpenQA.Selenium;

namespace GoWild.TestFramework
{
    public class HomePage
    {
        static string Url = "http://localhost:51745/";
        private static string PageTitle = "GoWild";

        public void GoTo()
        {
            Browser.GoTo(Url);
        }

        public bool Arrived()
        {
            return Browser.Title == PageTitle;
        }
    }
}
