﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoWild.TestFramework;

namespace GoWild.Tests
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void HomePageLoaded()
        {
            Pages.HomePage.GoTo();
            Assert.IsTrue(Pages.HomePage.Arrived());
        }

        [TestCleanup]
        public void Cleanup()
        {
            Browser.Close();
        }
    }
}
